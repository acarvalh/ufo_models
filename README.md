# UFO_models

If you use LXPLUS the necessary 

```
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_102 x86_64-centos7-gcc11-opt
```

- If you have rights in CMS Madgraph software can be found on

```
cd /afs/cern.ch/cms/generators/www/MG5_aMC_v3.3.1.tar.gz /my/work/dir
tar -xzf MG5_aMC_v3.3.1.tar.gz
```

To launch the MG executable do 

```
./MG5_aMC_v3_3_1/bin/mg5_aMC
```

The models bellow need to be copied (or soft copied) inside `MG5_aMC_v3_3_1/models/`

## loop_sm_scalar_mod

Originally from [NewScalars](https://gitlab.com/apapaefs/NewScalars), from Andreas Papaefstathiou .
Please cite XXX XXX 

It is the loop sm default with addition of one scalar with the parametrization XXX XXX.

```
cp -r ufo_models/loop_sm_scalar_mod MG5_aMC_v3_3_1/models/
mkdir teste_output_singlet
./MG5_aMC_v3_3_1/bin/mg5_aMC
```

If Madgraph is version 2.9.x you generate the different parts by

```
> generate g g > h h [QCD] ## SM-only
```

If  Madgraph is version 3.4.x you generate the different parts by

```
> generate g g > h h / eta0 [noborn=QCD]       ## SM-only
```

For the resonance-only part:

```
> generate g g > eta0, eta0 > h h [noborn=QCD] ## resonance-only 
```

-- that does not work on loop induced events [Bruno point the command]

To output 

- Modifications from Bruno Alves: 
    - added two coupling orders to be able to separate the resonant and non-resonant contributions
    - The summary of the modifications can be found in thse commits [ADD LINKS]

- Modifications from Alexandra Carvalho
    - Make kap111 = 31.72995208 by default 
        - the SM value, If computed using the equations on `loop_sm` [1]
    - added a multiplier to the trilinear coupling (`kapLam`)
    - The summary of the modifications can be found [ADD COMMIT]

Results: 
- For SM-only (ctheta = 1, stheta = 0, kapLam = 1): 
    - Cross-section :   0.04258 +- 3.302e-05 pb

- For SM-only (ctheta = 1, stheta = 0, kapLam = 10): 
    - Cross-section :   0.02634 +- 1.891e-05 pb

## loop_sm_kappas_to_VBF_HH

Originally from the default `loop_sm`

- Modifications from Alexandra Carvalho
    - Adding kappas to the couplings HHH, HVV and HHVV

Results: 
- For SM-only (complete): 
    - Cross-section :   0.01453 +- 1.132e-05 pb

- For SM-only (complete), klam = 10: 
    - Cross-section :   0.2423 +- 0.000961 pb


## loop_sm (defaut in MG)

```
./MG5_aMC_v3_3_1/bin/mg5_aMC
import model loop_sm
generate g g > h h / b  [noborn=QCD]
```

- Cross-section :   0.01449 +- 1.069e-05 pb